<?php
abstract class Publication{
	public $id;
	public $title;
	public $about;
	public $content;
	public $type;
	

    public function getId(){
        return $this->id;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getAbout(){
        return $this->about;
    }
    public function getContent(){
        return $this->content;
    }
    public function getType(){
        return $this->type;
    }


    public function getShortPreview(){
        echo '<h2>'.$this->getTitle().'</h2>';
        echo '<h3>'.$this->getType().'</h3>';
        echo '<p>'.$this->getAbout().'</p>';
        echo '<p><a class="btn btn-primary" href="?getcontent='.$this->getId().'" role="button">Читать...</a></p>';
    }
    public static function create(PDO $pdo,$id)
    {
        if($id <= 0){
            throw new Exception('Incorrect IDs');
        }
        if(!is_a($pdo,'PDO')){
            throw new Exception('Not connected to the database server.');
        }

        $sql = 'SELECT * FROM content where id = :id';
        $data = $pdo->prepare($sql);
        $data->execute(array(':id' => $id));
        $row = $data->fetchObject();


        $publication = '';
        if($row->type == 'article'){
            $publication = new Article(
                $row->id,
                $row->title,
                $row->about,
                $row->content,
                $row->type,
                $row->author
            );

        }else if($row->type == 'news'){
            $publication = new News(
                $row->id,
                $row->title,
                $row->about,
                $row->content,
                $row->type,
                $row->source
            );

        }
        return $publication;
    }

	public function __construct($id,$title,$about,$content,$type){
		$this->id = $id;
		$this->title = $title;
		$this->about = $about;
		$this->content = $content;
		$this->type = $type;
	}
}