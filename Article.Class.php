<?php 
class Article extends Publication
{

	public $author;

	public function __construct($id,$title,$about,$content,$type,$author){
		parent::__construct($id,$title,$about,$content,$type);
		$this->author = $author;
	}

}