<?php
class PublicationsWriter
{


	public $publications = array();
	public $x = 1;


	
	public function __construct($entryType,PDO $pdo){

		if ($entryType != 'article' and $entryType != 'news'){
			throw new Exception("Error Processing Request");
	        }
	    

		   $query = "SELECT * FROM content WHERE type =:entryType";
	       $stmt = $pdo->prepare($query);
	       
	       $stmt->execute(array('entryType' => $entryType));
	       $rows = $stmt->fetchAll(PDO::FETCH_CLASS);
	       
	       if ($entryType == 'news'){
		       foreach ($rows as $content) {
		       $this->publications[] = new News(
						       		$content->id,
						       		$content->title,
						       		$content->about,
						       		$content->content,
						       		$content->type,
						       		$content->source
						       		);
		       }

	   	   }
	   	   elseif ($entryType == 'article'){
				foreach ($rows as $content) {
		       	$this->publications[] = new Article(
						       		$content->id,
						       		$content->title,
						       		$content->about,
						       		$content->content,
						       		$content->type,
						       		$content->author
						       		);
		       }
		   	}
		   }
	   	  }



	       



