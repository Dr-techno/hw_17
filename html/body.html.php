<?php
if (isset($_GET['getcontent'])){
    $id = $_GET['getcontent'];

try {
    $publication = Publication::create($pdo,$id);
} catch (Exception $e) {
    echo $error = ' MESSAGE: ' . $e->getMessage() . '<br> FILE: ' . $e->getFile();
}

?>
    <h1>
        <?=$publication->getType()?>
    </h1>
    <p>
        <?=$publication->getContent()?>
    </p>


<?php
    }else{
?>
<div class="row">
    <div class="col-lg-6">
        <h2>Статьи</h2>
   <?php foreach ($articles as $index => $article):?>
       <?php foreach ($article as $index => $item):?>

                <div class="col-lg-6">
                    <?php $item->getShortPreview();?>
                </div>

        <?php endforeach;?>
    <?php endforeach;?>
    </div>

    <div class="col-lg-6">
         <h2>Новости</h2>
        <?php foreach ($news as $index => $new):?>
            <?php foreach ($new as $index => $item):?>

                <div class="col-lg-6">
                    <?php $item->getShortPreview();?>
                </div>

            <?php endforeach;?>
        <?php endforeach;?>
    </div>
</div>
<?php }?>